var obj={};
var counter=1,index,id,rescounter=0;

var newques=document.getElementById("newquestion");
newques.addEventListener("click",openForm);

var submit=document.getElementById("submit");
submit.addEventListener("click",getData)

var sub=document.getElementById("title");
var des=document.getElementById("description");

var resname=document.getElementById("name");
var rescomment=document.getElementById("comment");
let responses=document.getElementById("responses");

var list=document.getElementById("lists");

var resolve=document.getElementById("resolve");
resolve.addEventListener("click",removeFromList);

if(localStorage.getItem("data"))
{
  obj=JSON.parse(localStorage.getItem("data"));

  for(prop in obj)
    if(obj[prop].favour==true)
      addToList(prop);

  for(prop in obj)
    if(obj[prop].favour==false)
      addToList(prop);
}

function openForm(){
  document.getElementById("solution").style.display="none";
  document.getElementById("newform").style.display="block";
}

function getData(){
  if(sub.value=='' || des.value=='')
  {
    alert("Please Enter Both Fields");
    return;
  }
  id="q"+counter;
  obj[id]={};
  obj[id].title=sub.value;
  obj[id].description=des.value;
  obj[id].favour=false;
  obj[id].response=[];
  localStorage.setItem("data",JSON.stringify(obj));
  sub.value="";
  des.value="";
  addToList(id);
}
function addToList(id){
  let div=document.createElement("div");
  div.setAttribute("class","listing");
  div.addEventListener("click",showquestion);

  let h3=document.createElement("h3");
  h3.innerHTML=obj[id].title;
  
  let p=document.createElement("p");
  p.innerHTML=obj[id].description;
  
  let star=document.createElement("span");
  star.setAttribute("class","fa fa-star");
  star.classList.add("class","checked");
  star.addEventListener("click",makeFavourite);

  if(obj[id].favour==true)
    star.style.color="orange";
  else
    star.style.color="rgb(212, 205, 205)";

  div.setAttribute("name",id);
  div.append(h3,star,p);
  
  list.appendChild(div);
  counter++;
}

function showquestion(){
  let parent=document.getElementById("details");
  while(parent.lastChild)
    parent.removeChild(parent.lastChild);
  document.getElementById("newform").style.display="none";
  document.getElementById("solution").style.display="block";
  let p=document.createElement("p");
  p.innerHTML=this.innerHTML;
  parent.appendChild(p);

  let text=this.children[0].innerHTML;
  for(prop in obj)
  {
    if(text==obj[prop].title)
    {
      index=prop;
      break;
    }
  }
  while(responses.lastChild)
  {
    responses.removeChild(responses.lastChild);
  }
  responses.style.display="none";
  showResponses();
}  

function showResponses(){
  if(obj[index].response)
  {
    for(var i=0;i<obj[index].response.length;i++)
    {
        let div=document.createElement("div");
        let h3=document.createElement("h3");
        h3.innerHTML=obj[index].response[i].name;
        let p=document.createElement("p");
        p.innerHTML=obj[index].response[i].comment;

        div.appendChild(h3);
        div.appendChild(p);
        responses.appendChild(div);
        responses.style.display="block";
    }
    rescounter=i;
  }
  else
  {
    rescounter=0;
  }
}

let subcomment=document.getElementById("subcomment");
subcomment.addEventListener("click",addResponse);

function addResponse(){
    if(resname.value=="" || rescomment.value=="")
    {
      alert("Please fill both fields");
      return;
    }  
    let div=document.createElement("div");
    
    let h3=document.createElement("h3");
    h3.innerHTML=resname.value;
    obj[index].response[rescounter]={};
    obj[index].response[rescounter].name=resname.value;

    let p=document.createElement("p");
    p.innerHTML=rescomment.value;
    obj[index].response[rescounter].comment=rescomment.value;
    rescounter++;

    localStorage.setItem("data",JSON.stringify(obj));
    div.append(h3, p);
    responses.style.display="block";
    resname.value="";
    rescomment.value="";
    responses.appendChild(div);
}
function searching(){
  var input,filter,h3;
  input=document.getElementById("search");
  filter=input.value.toUpperCase();
  let div=document.getElementsByClassName("listing");
  for(let i=0;i<div.length;i++)
  {
    h3=div[i].children[0];
    
    if(h3.textContent.toUpperCase().indexOf(filter) > -1) 
    {
      div[i].style.display = "block";
    } 
    else 
    {
      div[i].style.display = "none";
    }
  }
}

function removeFromList(){
  let div=document.getElementsByName(index);
  list.removeChild(div[0]);
  delete obj[index];
  localStorage.setItem("data",JSON.stringify(obj));
  openForm();
}

function makeFavourite(){
  let div=this.parentElement;
  let check=div.getAttribute("name");
  if(obj[check].favour==false)
  {
    obj[check].favour=true;
    this.style.color="orange";
  }
  else
  {
    obj[check].favour=false;
    this.style.color="rgb(212, 205, 205)";
  }
  localStorage.setItem("data",JSON.stringify(obj));
}